import settings

class TransaccionDatos:
    
    def __init__(self):
        self.__conn = settings.conn

    def insertarTransaccion(self, idBus, dniPersona, cantidad):
        query = "INSERT INTO transaccion(idBus, dniPersona, cantidad) VALUES (%s,%s,%s)"
        args = (idBus, dniPersona, cantidad) 

        try:
            self.__conn.execute(query, args)
        except Exception as err:
            print(err.__str__())
        
    def comprobarExistencias(self, dni):
        query = "SELECT count(*) FROM transaccion WHERE dniPersona LIKE '%s'" 
        args = (dni)

        try:
            transaccion = self.__conn.mostrarUno(query, args)
            return transaccion
        except Exception as err:
            print(err.__str__())