import settings

class personaDatos:
	def __init__(self):
		self.__conn = settings.conn
		

	def tryExecute(self, query, args = None):
		if(args != None):

			try:
				self.__conn.execute(query, args)
			except Exception as err:
				print(err.__str__())
		else:
			try:
				self.__conn.execute(query)
			except Exception as err:
				print(err.__str__())

	def insertarPersona(self, dni, nombre, edad):
		query = "INSERT INTO persona(dni,nombre,edad) VALUES(%s, %s, %s)" 
		args = (dni, nombre, edad)
		self.tryExecute(query, args)

	def listaPersonas(self):
		query = "SELECT * FROM persona"
		try:
			personas = self.__conn.mostrarLista(query)
			return personas
		except Exception as err:
			print(err.__str__())