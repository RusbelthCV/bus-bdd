class Bus:
    def __init__(self,nombre,asientosTotales):
        self.__nombre=nombre
        self.__asientosTotales=asientosTotales
        self.__asientosDisponibles=asientosTotales

    def getNombre(self):
        return self.__nombre

    def setNombre(self,nombre):
        self.__nombre=nombre

    def getPlazasTotales(self):
        return self.__asientosTotales

    def setPlazasTotales(self,asientosTotales):
        self.__asientosTotales=asientosTotales

    def getAsientosDisponibles(self):
        return self.__asientosDisponibles

    def setAsientosDisponibles(self,asientosDisponibles):
        self.__asientosDisponibles=asientosDisponibles
