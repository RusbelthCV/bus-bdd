from models.connection import Conexion
from models.bus import Bus
from models.busDatos import BusDatos
from models.Persona import Persona
from models.personaDatos import personaDatos
from models.Transaccion import Transaccion
from models.transaccionDatos import TransaccionDatos
import settings

settings.init()

busBdd = BusDatos()
personaBdd = personaDatos()
transaccionBdd = TransaccionDatos()

def conectar():
    conectado = busBdd.comprobarConexion()
    if(conectado == True):
        print("Se ha conectado a la base de datos.")
    else:
        print("No se ha podido conectar a la base de datos.")
    return conectado
    
def crearBus():
    
    nombre = input("Nombre para registrar el bus: ")
    while True:
        try:
            plazas = int(input("Plazas totales en el bus: "))
            break
        except Exception as err:
            print("No ha introducido un valor númerico.")
    bus = Bus(nombre,plazas)
    busBdd.insertarBus(bus)

def borrar():
    buses=busBdd.listar_buses()
    for x in range(len(buses)):
        print(f"{x+1}.-Nombre:{buses[x][1]} plazas{buses[x][2]} asientos libres:{buses[x][3]}")
    eleccion= int(input("Elige un bus: "))
    busBdd.borrar(buses[eleccion-1][0])

def modificar():
    buses=busBdd.listar_buses()
    for x in range(len(buses)):
            print(f"{x+1}.-Nombre:{buses[x][1]} plazas{buses[x][2]} asientos libres:{buses[x][3]}")
    eleccion= int(input("Elige un bus: "))
    nombre=input("Nuevo nombre: ")
    print(buses[eleccion-1][0])
    busBdd.modificar(buses[eleccion-1][0], nombre)
def vender():
    buses=busBdd.listar_buses()
    for x in range(len(buses)):
            print(f"{x+1}.-Nombre:{buses[x][1]} plazas{buses[x][2]} asientos libres:{buses[x][3]}")
    eleccion= int(input("Elige un bus: "))
    print(f"Las plazas disponibles son: {buses[eleccion-1][3]} ")
    tickets=int(input("Cuantos billetes va a comprar "))
    if tickets > buses[eleccion-1][3] and tickets <= 0:
        print("Operacion incorrecta")
    else:
        busBdd.comprarBilletes(buses[eleccion-1][0], tickets,buses[eleccion-1][3])

def listar_buses():
    buses=busBdd.listar_buses()
    for x in range(len(buses)):
        print(f"{x+1}.-Nombre:{buses[x][1]} plazas{buses[x][2]} asientos libres:{buses[x][3]}")

def menuUpdate(idBus):

    print("---------- MENU UPDATE ----------")
    print("1.- Cambiar nombre\n2.-Cambiar plazas totales")

    while True:
        try:
            eleccion = int(input("Selecciona la opción: "))
            break
        except Exception as err:
            print(err.__str__())
    if (eleccion == 1):
        nombreNuevo = input("Selecciona un nombre nuevo: ")
        busBdd.cambiarNombre(idBus, nombreNuevo)
    elif (eleccion == 2):
        while True:
            try:
                plazasNuevas = int(input("Selecciona el valor total de plazas: "))
                break
            except Exception as err:
                print(err.__str__())
        busBdd.cambiarPlazas(idBus, plazasNuevas)

def update():
    buses=busBdd.listar_buses()
    for x in range(len(buses)):
        print(f"{x+1}.-Nombre:{buses[x][1]} plazas{buses[x][2]} asientos libres:{buses[x][3]}")
    while True:

        try:
            eleccion = int(input("Elige un bus: "))
            break
        except ValueError as err:
            print("No ha insertado un valor númerico")
    menuUpdate(buses[eleccion - 1][0])

def pedirBilletes(bus, devolucion, dni):
    
    while True:
        try:
            billetes = int(input("Número de billetes a comprar en este bus: "))
            break
        except ValueError:
            print("No ha seleccionado un valor númerico.")
    
    transaccion = Transaccion(bus[0], dni, billetes)

    existencias = transaccionBdd.comprobarExistencias(dni)
    if existencias[0] == 0:

        if(devolucion == False):  #Venta
            if(billetes <= bus[3] and billetes > 0):
                transaccionBdd.insertarTransaccion(transaccion.getIdBus(), transaccion.getDniPersona(), transaccion.getCantidad())
                busBdd.operacionBilletes(billetes,bus[3], bus[0], devolucion)
        #else: #Devolución
            
    
    #TODO ---> Acá va un update a una transacción ya existente
            

def operacionBilletes(devolucion, dni):
    buses=busBdd.listar_buses()
    
    for x in range(len(buses)):
        print(f"{x+1}.-Nombre:{buses[x][1]}  asientos libres: {buses[x][3]}")
    while True:
        try:
            eleccion = int(input("Seleccione el bus al que quiere comprar billetes: "))
            break
        except ValueError as err:
            print("No ha seleccionado un valor númerico.")
    
    if(eleccion <= len(buses) and eleccion > 0):
        pedirBilletes(buses[eleccion - 1], devolucion, dni)
    else:
        print("Ha seleccionado un bus inexistente")

def menuPasajero(dni):
    respuesta = 1
    print("---------- MENU PASAJERO ----------")

    while respuesta > 0:
        print("1.-Comprar billetes\n2.-Devolver billetes\n3.- Mis billetes")
        while True:
            try:
                respuesta = int(input("Eleccion: "))
                break
            except ValueError:
                print("Introduzca un valor númerico.")
            
        if(respuesta == 1):
            operacionBilletes(False, dni) 
        busBdd.commit()
        

def crearPersona():

    dni = input("Dni: ")
    nombre = input("Nombre: ")
    while True:
        try:
            edad = int(input("Edad: "))
            break
        except ValueError:
            print("Introduzca un valor númerico.")
    persona = Persona(dni, nombre, edad)
    personaBdd.insertarPersona(persona.getDni(), persona.getNombre(), persona.getEdad())
    menuPasajero(persona.getDni())
def listaPersonas():
    personas = personaBdd.listaPersonas()

    return personas

def elegirPersona():
    personas = listaPersonas()

    for x in range(len(personas)):
        print(f"{x + 1}.- Dni: {personas[x][0]} ")
    while True:
        try:

            eleccion = int(input("Elige la posición de tu dni: "))
            break
        except ValueError:
            print("Debes insertar un valor númerico.")
    if eleccion > 0  and eleccion <= len(personas):
        menuPasajero(personas[eleccion - 1][0])

def menuBus():
    respuesta = 3
    while respuesta > 0:
        print("---------- MENU BUS ----------")
        print("1.- Registrar Bus\n2.-Lista de buses\n3.-Borrar bus\n4.-Modificar un bus\n5.-Registrar Persona\n6.-Entrar como pasajero\
            \n7.-Lista de pasajeros")
        while True:
            try:
                respuesta = int(input("Selección: "))
                break
            except ValueError as err:
                print("No ha insertado un valor númerico.")
        if (respuesta == 1):
            crearBus()
        elif respuesta==2:
            listar_buses()
        elif respuesta==3:
            borrar()
        elif (respuesta == 4):
            update()
        elif (respuesta == 5):
            crearPersona()

        elif (respuesta == 6):
            #operacionBilletes(True) Devolución
            elegirPersona()

        elif(respuesta == 7):
            personas = listaPersonas()

            for x in range(len(personas)):
                print(f"{x + 1}.- Dni: {personas[x][0]} Nombre: {personas[x][1]} Edad: {personas[x][2]}")
        
        busBdd.commit()
    busBdd.closeConnections()

if __name__ == "__main__":
    conectado = conectar()
    if(conectado == True):  
        menuBus()