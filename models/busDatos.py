import settings

class BusDatos:
  def __init__(self):
    self.__conn = settings.conn

  def comprobarConexion(self):
    resultado = self.__conn.comprobarConexion()

    return resultado
  
  def cambiarNombre(self,idBus,nombre):
    try:
      query = "UPDATE bus SET nombre = %s WHERE id = %s" % (nombre, idBus)
      self.__conn.execute(query)
    except Exception as err:
      print(err.__str__())

  def cambiarPlazas(self,idBus,plazas):
    try:
      query = "UPDATE bus SET plazas = %s WHERE id = %s" % (plazas, idBus)
      self.__conn.execute(query)
    except Exception as err:
      print(err.__str__())

  def insertarBus(self,bus):
    try:
      query = "INSERT INTO bus(nombre,plazas,disponibles) VALUES(%s,%s,%s)"
      args = (bus.getNombre(), bus.getPlazasTotales(), bus.getPlazasTotales())

      self.__conn.execute(query, args)
    except Exception as err:
      print(err.__str__())

  def borrar(self,idbus):
      try:
          query="DELETE  FROM bus where id=%s" % (idbus)
          self.__conn.execute(query)
      except Exception as err:
          print(err.__str__())

  def listar_buses(self):
    try:
        query="SELECT * FROM bus"
        buses=self.__conn.mostrarLista(query)
        return buses
    except Exception as err:
        print(err.__str__())
  def operacionBilletes(self,billetes,disponibles, idBus, devolucion = False):
    if(devolucion == False):
      total = disponibles - billetes
    else:
      total = disponibles + billetes
    try:
      query = "UPDATE bus SET disponibles = %s WHERE id = %s" % (total, idBus)
      self.__conn.execute(query)
    except expression as identifier:
      pass

  def commit(self):
    self.__conn.commit()

  def closeConnections(self):
    self.__conn.closeConnections()

