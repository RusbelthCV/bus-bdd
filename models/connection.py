import mysql.connector

class Conexion:

    def __init__(self,host,db,user,passwd):
        try:
            self.__conn = mysql.connector.connect(host=host, user = user, password = passwd, database = db)
            self.__cur = self.__conn.cursor()

        except Exception as err:
            print(err.__str__())
            self.__conn = None
            
    def execute(self, query, args = None):
        if(args == None):
            try:
                self.__cur.execute(query)
            except Exception as err:
                print(err.__str__())
        else:
            try:
                self.__cur.execute(query, args)
            except Exception as err:
                print(err.__str__())

    def closeConnections(self):
        self.__conn.close()
        self.__cur.close()
    def commit(self):
        self.__conn.commit()
    
    def comprobarConexion(self):
        if(self.__conn != None):
            conectado = True

        else:
            conectado = False

        return conectado

    def mostrarLista(self,query):
        try:
            self.__cur.execute(query)
            lista=self.__cur.fetchall()
            return lista
        except Exception as err:
            print(err.__str__())

    def mostrarUno(self, query, args):
        try:
            self.__cur.execute(query, args)
            lista=self.__cur.fetchone()
            return lista
        except Exception as err:
            print(err.__str__())