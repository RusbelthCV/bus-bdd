class Transaccion:
	def __init__(self, idBus, dniPersona, cantidad):
		self.__idBus = idBus
		self.__dniPersona = dniPersona
		self.__cantidad = cantidad

	def getIdBus(self):
		return self.__idBus

	def getDniPersona(self):
		return self.__dniPersona

	def getCantidad(self):
		return self.__cantidad
